package paquete.wachav2;

import java.util.ArrayList;

public class Mensaje {	
	String nombre;	//Nombre de usuario
	ArrayList<String> cadena;	//Array de mensajes enviados
	public Mensaje(String string, String string2) {
		cadena = new ArrayList<String>();  	//Crea el array de strings que almacenar� los mensajes
		nombre = string;					// Coloca nombre
		cadena.add(string2);				// A�ade cadena
	}
	public void nuevoMsj(String string) {
		cadena.add(string);	//A�ade nuevo mensaje al array
	}
	public String nombre() {
		return this.nombre;
	}
	public String cadena() {
		String result = null; //cadena donde almacenar los mensajes
		for (String aux : this.cadena)
			result = result + aux + "; ";
		return result;
	}
	

}
