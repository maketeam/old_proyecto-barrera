package paquete.wachav2;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.Socket;
import java.util.ArrayList;

public class ServidorHilos implements Runnable {

	ArrayList<Usuario> chatArray;
	Socket socket;
	String nombre;
	
	public ServidorHilos ( Socket socket, int idsession, ArrayList<Usuario> usuario) {
		this.chatArray = usuario;
		this.socket = socket;
		
	}
	public void run() {
		//Aqui se implementa la rutina del servidor
		boolean sesion = true;	//Sesi�n continua abierta
		DataInputStream flujoIn = this.insocket(socket);	//inicia socket y handler de flujo de entrada.
		DataOutputStream flujoOut = this.outsocket(socket);
		this.nombre = this.inicio(flujoIn, flujoOut); //Espera a que el cliente inicie el proceso de alta
		this.alta(this.nombre, this.chatArray);
		while(sesion)
			sesion = this.espera(flujoIn, flujoOut,chatArray);
		this.cierra(socket, flujoIn, flujoOut);
		
	}
	private void cierra(Socket socket2, DataInputStream flujoIn, DataOutputStream flujoOut) {
		//cierra sockets y streams
		try {
			socket2.close();
			flujoIn.close();
			flujoOut.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	private boolean espera(DataInputStream flujoIn, DataOutputStream flujoOut, ArrayList<Usuario> chatArray2) {
		String accion;
		boolean sesion = true;
		try {
			accion = flujoIn.readUTF();
			switch (accion.charAt(0)) {
			case '2':
				this.escribe(chatArray2,accion);
				break;
			case '3':
				this.lee(chatArray2,flujoOut);
				break;
			case '0':
				sesion=false;
				break;
			default:
				break;
			}
			
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return sesion;
	}
	synchronized void lee(ArrayList<Usuario> chatArray2, DataOutputStream flujoOut) throws IOException {
		// Devuelve los mensajes correspondientes al usuario y los env�a
		String mensajes = null;
		for (Usuario aux : chatArray2) {
			if (aux.getNombre() == this.nombre) {
				mensajes = aux.leer();
				break;
			}
		}
		mensajes = mensajes + "FINAL;";
		flujoOut.writeUTF(mensajes);
	}
	synchronized void escribe(ArrayList<Usuario> chatArray2, String accion) {
		String nombre;
		String mensaje;
		int separador = accion.indexOf(":");
		nombre = accion.substring(1, separador-1);
		mensaje = accion.substring(separador+1);
		for (Usuario aux : chatArray2){
			if (aux.getNombre() == this.nombre){
				aux.nuevoMsj(nombre, mensaje);
			}
		}
	}
	synchronized void alta(String nombre2, ArrayList<Usuario> chatArray2) {
		chatArray2.add(new Usuario(nombre2));
	}
	private DataOutputStream outsocket(Socket socket2) {
			OutputStream aux0 = null;
			DataOutputStream salida = null;
			while( salida == null){
				try {
					aux0 = socket2.getOutputStream();
					salida = new DataOutputStream(aux0);
				} catch (IOException e) {
					// Guarrerida autogenerada :)
					e.printStackTrace();
				}
			}
			return salida;
	}
	private DataInputStream insocket(Socket socket2) {	
		InputStream aux0 = null;
		DataInputStream entrada = null;
		while(entrada == null){
			try {
				aux0 = socket2.getInputStream();
				entrada = new DataInputStream(aux0);
			} catch (IOException e) {
				// Guarrerida autogenerada :)
				e.printStackTrace();
			}
		}
		return entrada;
	}
	
	private String inicio(DataInputStream flujoIn, DataOutputStream flujoOut) {
		String inicio = "inicial";
		try {
			inicio = flujoIn.readUTF();
			while (inicio.charAt(0) != '1')
			{	
				flujoOut.writeUTF("FAIL");
				inicio = flujoIn.readUTF();
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return inicio.substring(1);
	}

}
