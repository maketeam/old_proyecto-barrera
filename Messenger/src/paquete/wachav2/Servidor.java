package paquete.wachav2;

import java.io.*;
import java.net.*;
import java.util.ArrayList;
import java.util.logging.*;

public class Servidor {
//COMENTAIRE
    
    public static void main(String args[]) throws IOException {
        
    	ServerSocket ss;			//referencia para el socket
        ArrayList<Usuario> chat;	//referencia para el chat
        
        final int puerto = 60010;	//constante para puerto
        
        System.out.print("Inicializando servidor... ");
        
        try {
            ss = new ServerSocket(puerto);		//Creaci�n del socket(puerto)
            chat = new ArrayList<Usuario>();	//Creaci�n del chat
            
            System.out.println("\t[OK]");
            
            int idSession = 0;
            while (true) {
                Socket socket;
                socket = ss.accept();
                System.out.println("Nueva conexi�n entrante: "+socket);
                new ServidorHilos(socket, idSession, chat).run();
                idSession++;
            }
        } catch (IOException ex) {
            Logger.getLogger(Servidor.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}