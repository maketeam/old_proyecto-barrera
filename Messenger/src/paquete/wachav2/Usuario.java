package paquete.wachav2;

import java.util.ArrayList;

public class Usuario {

	String nombre; //nombre de usuario
	ArrayList<Mensaje> input;
	
	public Usuario(String nombre){
		this.nombre = nombre;
		input = new ArrayList<Mensaje>();
	}
	
	public String getNombre() {
		return nombre;
	}

	public void nuevoMsj(String nombre, String msg) {
		Boolean nuevo = true;
		for (Mensaje auxi : this.input) {
			if (auxi.nombre() == nombre) {
				auxi.nuevoMsj(msg);			//Ya existe el chat
				nuevo = false;
				break;
				}
			}
		if(nuevo)	
			 this.input.add(new Mensaje(nombre, msg));	//Nuevo chat == Primer mensaje
		
	}
	
	public String leer() {
		String msg = null;
		for (Mensaje aux : this.input)
			msg = msg + aux.nombre() + aux.cadena() + "; ";
		return msg;
	}

	
}
