package paquete.wachav2;

import junit.framework.TestCase;

public class MensajeTest extends TestCase {
	
	Mensaje msg;
	public void testConstructor() {
		Mensaje msg = new Mensaje("David","Mensaje de prueba");
		assertEquals("David", msg.nombre());
		assertEquals("Mensaje de prueba", msg.cadena());
	}

	public void testAnadir() {
		Mensaje msg = new Mensaje("David","msg1");
		msg.nuevo("msg2");
		assertEquals("1: msg1;2: msg2;",msg.mensajes);
	}
}
